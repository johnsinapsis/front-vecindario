import { combineReducers } from "redux";
import loginReducer from './login.slice'
import postsReducer from './post.slice'

const rootReducer = combineReducers({
    login: loginReducer,
    posts: postsReducer
})

export default rootReducer;