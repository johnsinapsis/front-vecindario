import styled from 'styled-components';


export const Box = styled.div`
padding: 2px 2px;
background: #808080;
display: block;
bottom: 0;
width: 100%;
/* margin-top: 100%; */


@media (max-width: 1000px) {
	padding: 70px 30px;
}
`;

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	max-width: 1000px;
	margin: 0 auto;
	/* background: red; */
`

export const Column = styled.div`
display: flex;
flex-direction: column;
text-align: center;
margin-left: 0px;
`;

export const Row = styled.div`
display: grid;
/*grid-template-columns: repeat(auto-fill,
						minmax(185px, 1fr));*/
grid-template-columns: 1fr 1fr 1fr 1fr;
grid-gap: 0px;
text-align: center;

/* @media (max-width: 1000px) {
	grid-template-columns: repeat(auto-fill,
						minmax(200px, 1fr));
} */
`;

export const FooterLink = styled.a`
color: #fff;
margin-bottom: 20px;
font-size: 18px;
text-decoration: none;

&:hover {
	color: green;
	transition: 200ms ease-in;
}
`;

export const Heading = styled.p`
font-size: 16px;
color: #fff;
margin-bottom: 10px;
font-weight: bold;

@media (max-width: 1000px){
	font-size: 0.8em;
}

`;
