import React from "react";
import { screen,render } from "@testing-library/react";
import Login from './Login'
import {Provider} from 'react-redux'
import store from '../../redux/store'
import { getSessionFailure } from '../../redux/slices/login.slice'
//import { prettyDOM } from "@testing-library/react";


describe('Login',()=>{
    it('Validate credentials error',()=>{
        
   
  const Wrapper = (props)=>{
      return(
        <Provider store={store}>
            <Login></Login>
        </Provider>
      )
  }
  store.dispatch(getSessionFailure({response:{error:"Las credenciales no son válidas"}}))
  //let state = store.getState().login
  //console.log(state.response);
  
        /* const component = render(<Wrapper/>)
        const container = component.container
        console.log(prettyDOM(container)); */
        render(<Wrapper/>)
        expect(screen.getByText(/Las credenciales no son válidas/i)).toBeInTheDocument()
    })
})