import styled from "styled-components";

export const StyleLoginSmall = styled.small`
    font-weight: 400;
    line-height: 1;
    color: #777;
`